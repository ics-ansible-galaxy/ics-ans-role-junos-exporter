# ics-ans-role-junos-exporter

Ansible role to install junos-exporter.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-exporter
```

## License

BSD 2-clause
