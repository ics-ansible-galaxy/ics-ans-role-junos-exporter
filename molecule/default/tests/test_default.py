import os
import testinfra.utils.ansible_runner
import re

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    service = host.service('junos_exporter')
    assert service.is_running
    assert service.is_enabled


def test_config(host):
    conf_file = host.file('/etc/junos_exporter/junos_exporter.yaml').content_string
    assert re.search(r'default:\n    auth:\n', conf_file) is not None
